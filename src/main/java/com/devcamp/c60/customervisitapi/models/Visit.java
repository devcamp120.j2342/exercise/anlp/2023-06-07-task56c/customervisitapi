package com.devcamp.c60.customervisitapi.models;

import java.sql.Date;
import java.time.LocalDate;

public class Visit{
    private Customer customer;
    private Date date;
    private double serviceExpence;
    private double productExpence;
    
    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }

    public String getName() {
        return customer.getName();
    }

    public double getServiceExpence() {
        return serviceExpence;
    }

    public void setServiceExpence(double serviceExpence) {
        this.serviceExpence = serviceExpence;
    }

    public double getProductExpence() {
        return productExpence;
    }

    public void setProductExpence(double productExpence) {
        this.productExpence = productExpence;
    }

    public double getTotalExpence() {
        return serviceExpence + productExpence;
    }

    public Date getDate() {
        LocalDate localDate = LocalDate.of(2023, 6, 7);
        Date date = Date.valueOf(localDate);
        return date;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + getDate() + ", serviceExpence=" + serviceExpence
                + ", productExpence=" + productExpence + "]";
    }

    
}
