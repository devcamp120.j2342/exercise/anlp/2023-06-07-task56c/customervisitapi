package com.devcamp.c60.customervisitapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.c60.customervisitapi.models.Customer;

@Service
public class CustomerService {

    Customer customer1 = new Customer("Nguyen Van A");
    Customer customer2 = new Customer("Nguyen Van B");
    Customer customer3 = new Customer("Nguyen Van C");

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = new ArrayList<Customer>();

        allCustomer.add(customer1);
        allCustomer.add(customer2);
        allCustomer.add(customer3);
        return allCustomer;
    }
}
